#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct list
{
	char str[10]; 
	int num;
	struct list *next;
}list_t;

list_t *head = NULL;

int tonum(char buf[10])
{
	int len, n = 0, i;
	len = strlen(buf);
	for(i = 2; i < len; i++)
	{
		n = (n * 10) + (buf[i] - '0');
	}
	return n;
}

void add(char buf[10])
{
	list_t *new, *temp;
	new = (list_t *)malloc(sizeof(list_t));
	temp = head;

	strcpy(new->str, buf);
	new->num = tonum(buf);

	if(head == NULL)
	{
		head = new;
		head->next = NULL;
	}
	else
	{
		while(temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = new;
		new->next = NULL;
	}
}

void print(list_t *param)
{
	list_t *temp;
	temp = param;
	while(temp != NULL)
	{
		printf("%s\n", temp->str);
		temp = temp->next;
	}
}

void Sort()
{
	struct list *outer, *inner, *comp, *cNode, *aNode;
	outer = head;
	int i, count = 0;

	while(outer->next != NULL)
	{
		count++;
		outer = outer->next;
	}

	for(i = 0; i < count; i++)
	{
		inner = head;
		aNode = NULL;
		while(inner->next != NULL)
		{
			comp = inner->next;
			cNode = comp->next;
			if((inner->str[0] > comp->str[0]) || ((inner->str[0] == comp->str[0]) && (inner->str[1] > comp->str[1])) 
				|| ((inner->str[0] == comp->str[0]) && (inner->str[1] == comp->str[1]) && (inner->num > comp->num)))
			{
				inner->next = cNode;
				comp->next = inner;
				if(inner == head)
				{
					head = comp;
				}
				else
				{
					aNode->next = comp;
				}
				aNode = inner;
				inner = comp;	
			}
			else
			{
				aNode = inner;
				inner = inner->next;
			}
		}
	}
} 

int main(void)
{
	add("-c10");
	add("*a10");
	add("-a10");
	add("-a1");
	add("-a2");
	add("=a3");
	add("-b2");
	add("-b100");
	add("-b23");
	add("-b99");
	printf("Before Sorting:\n");
	print(head);
	Sort();
	printf("\nAfter Sorting:\n");
	print(head);
	return 0;
}