#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct farmer
{
	char province[32];
	int c, d, e, f, g, h, i, j, k;
	struct farmer *next;
}farmer_t;

farmer_t *head = NULL;

int tonum(char buf[10])
{
	int len, n = 0, i;
	len = strlen(buf)-1;
	for(i = 0; i < len; i++)
	{
		n = (n * 10) + (buf[i] - '0');
	}
	return n;
}

void add(char buf[10], int c, int d, int e, int f, int g, int h, int i, int j, int k)
{
	farmer_t *new, *temp;
	new = (farmer_t *)malloc(sizeof(farmer_t));
	temp = head;

	strcpy(new->province, buf);
	new->c = c;
	new->d = d;
	new->e = e;
	new->f = f;
	new->g = g;
	new->h = h;
	new->i = i;
	new->j = j;
	new->k = k;

	if(head == NULL)
	{
		head = new;
		head->next = NULL;
	}
	else
	{
		while(temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = new;
		new->next = NULL;
	}
}

void femnum(farmer_t *temp)
{
	char cc[32], dd[32], ee[32], ff[32], gg[32], hh[32], ii[32], jj[32], kk[32];
	int cnt = 0, i, ca, ba, da, ea, fa, ga, ha, ia, ja, ka;
	farmer_t *tp, *param;
	param = temp->next;
	ca = param->c;
	da = param->d;
	ea = param->e;
	fa = param->f;
	ga = param->g;
	ha = param->h;
	ia = param->i;
	ja = param->j;
	ka = param->k;
	while(param != NULL && param->next != NULL)
	{
		if(param->next->c > ca)
		{
			ca = param->next->c;
			strcpy(cc, param->next->province);
		}
		if(param->next->d > da)
		{
			da = param->next->d;
			strcpy(dd, param->next->province);
		}
		if(param->next->e > ea)
		{
			ea = param->next->e;
			strcpy(ee, param->next->province);
		}
		if(param->next->f > fa)
		{
			fa = param->next->f;
			strcpy(ff, param->next->province);
		}
		if(param->next->g > ga)
		{
			ga = param->next->g;
			strcpy(gg, param->next->province);
		}
		if(param->next->h > ha)
		{
			ha = param->next->h;
			strcpy(hh, param->next->province);
		}
		if(param->next->i > ia)
		{
			ia = param->next->i;
			strcpy(ii, param->next->province);
		}
		if(param->next->j > ja)
		{
			ja = param->next->j;
			strcpy(jj, param->next->province);
		}
		if(param->next->k > ka)
		{
			ka = param->next->k;
			strcpy(kk, param->next->province);
		}
		param = param->next;
	}
	printf("Province with most number of females: %s\n", ee);
	printf("Average number of females in all provinces: %f\n", (float)(temp->e)/20);
	printf("Most number of population (Column C): %s\n", cc);
	printf("Most number of population (Column D): %s\n", dd);
	printf("Most number of population (Column E): %s\n", ee);
	printf("Most number of population (Column F): %s\n", ff);
	printf("Most number of population (Column G): %s\n", gg);
	printf("Most number of population (Column H): %s\n", hh);
	printf("Most number of population (Column I): %s\n", ii);
	printf("Most number of population (Column J): %s\n", jj);
	printf("Most number of population (Column K): %s\n", kk);
}

int main(void)
{
	FILE *f;
	char temp[256], temp1[256], prov[32];
	char s[2] = ",";
	char *token;
	int cnt, num, flag = 0, a[9];
	f = fopen("farmer.csv", "r");
	if(!f)
	{
		printf("Error!\n");
		return 1;
	}
	while(fgets(temp, 256, f) != NULL)
	{
		if(flag > 3)
		{
			cnt = 0;
			token = strtok(temp, s);
			strcpy(prov, token);
			while(token != NULL)
			{
				cnt++;
				if(cnt > 2)
				{
					strcpy(temp1, token);
					if(cnt == 11)
					{
						temp1[strlen(temp1)-2] = '\0';
					}
					a[cnt-3] = tonum(temp1);
				}
				token = strtok(NULL, s);
			}
			if(flag%4 == 0)
			{
				add(prov, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]);
			}
		}
		flag++;
	}
	fclose(f);
	femnum(head);
	return 0;
}