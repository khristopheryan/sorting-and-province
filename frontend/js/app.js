var myapp = angular.module('app', []);

myapp.config(['$routeProvider',
	function($routeProvider) {
    	$routeProvider.
      	when('/home', {
			templateUrl: 'home.html',
			controller: 'homeController'
      	}).
      	when('/checkout', {
			templateUrl: 'checkout.html',
			controller: 'checkoutController'
      	}).
      	when('/receipt', {
			templateUrl: 'receipt.html',
			controller: 'receiptController'
      	}).
      	otherwise({
		redirectTo: '/home'
	});
}]);


myapp.controller('homeController', function($scope) {
	$scope.message = 'Home';
});

myapp.controller('checkoutController', function($scope) {
	$scope.message = 'Checkout';
});

myapp.controller('receiptController', function($scope) {
	$scope.message = 'Finish Shopping';
});