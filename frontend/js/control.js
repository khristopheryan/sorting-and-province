var stock = [50, 50, 50, 50, 50, 50, 50, 50, 50, 50],

function CartForm($scope) {

    $scope.store = {
        products: [{title: "Anashia", price: 1000, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image1"},
                   {title: "Kila's Quest", price: 450, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image2"},
                   {title: "Temple of Souls", price: 1000, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image3"},
                   {title: "Katos", price: 670.20, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image4"},
                   {title: "Fallen Angels", price: 988, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image5"},
                   {title: "The 6th Key", price: 320, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image6"},
                   {title: "Rako's Lair", price: 1300, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image7"},
                   {title: "Wishmaker", price: 158, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image8"},
                   {title: "Aliana's Realm", price: 2900, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image9"},
                   {title: "Ora's Legacy", price: 1100, stock: 50, desc: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus diam mi, vestibulum quis ante ut, rutrum pharetra nulla. In lectus ante, tincidunt non molestie ut, consectetur sed quam.", src: "image10"}]
    },          

    $scope.invoice = {
        items: [],
    },

    $scope.addItem = function (w,x,y,z,a) {
        $scope.invoice.items.push({
            qty: 1,
			sorc: w,
            description: x,
            cost: y,
            detail: z,
            id: a
        });
        localStorage.setItem("books", JSON.stringify($scope.invoice.items));

    },

    $scope.retrieve = function() {
        $scope.invoice.items = JSON.parse(localStorage.getItem("books"));
        for(x in $scope.invoice.items){
            console.log($scope.invoice.items[x]);
        }
    },

    $scope.removeItem = function (buf) {
        for(x in $scope.invoice.items) {
            if($scope.invoice.items[x].description == buf){
                $scope.invoice.items.splice(x, 1);
            }
        }
        localStorage.setItem("books", JSON.stringify($scope.invoice.items))
    },

    $scope.clearloc = function() {
        var temp;
        for(x in $scope.invoice.items){
            for(y in $scope.store.products){
                if($scope.invoice.items[x].description == $scope.store.products[y].title){
                    $scope.store.products[y].stock = $scope.store.products[y].stock - 1;
                    break;
                }
            }
        }

        $scope.invoice.items = [];
        localStorage.setItem("books", JSON.stringify($scope.invoice.items));
        temp = JSON.parse(localStorage.getItem("funds")) - JSON.parse(localStorage.getItem("total"));
        localStorage.setItem("funds", JSON.stringify(temp));

        return $scope.store.products;
    }

    $scope.total = function () {
        var total = 0;
        angular.forEach($scope.invoice.items, function (item) {
            total += item.qty * item.cost;
        })
        localStorage.setItem("total", JSON.stringify(total))
        return total;
    },

     $scope.my = function () {
        var total = 0;
        angular.forEach($scope.invoice.items, function (item) {
            total += 1;
        })

        if(total > 1)
        {
            total = total + " items";
        }
        else
        {
            total = total + " item";
        }

        return total;
    },

    $scope.validate = function () {
        var name = document.forms["info"]["Username"].value;
        var funds = document.forms["info"]["Funds"].value;
        localStorage.setItem("name", JSON.stringify(name));
        localStorage.setItem("funds", JSON.stringify(funds));
    }

    $scope.custom = function() {
        false;
	}
}