#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct exchange
{
	char id[10];
	float a, b, c, d, e, f, g, h, i;
	struct exchange *next;
}exchange_t;

exchange_t *head = NULL;

void add(char ida[10], float aa, float ba, float ca, float da, float ea, float fa, float ga, float ha, float ia)
{
	exchange_t *new, *temp;
	new = (exchange_t *)malloc(sizeof(exchange_t));
	temp = head;

	strcpy(new->id, ida);
	new->a = aa;
	new->b = ba;
	new->c = ca;
	new->d = da;
	new->e = ea;
	new->f = fa;
	new->g = ga;
	new->h = ha;
	new->i = ia;

	if(head == NULL)
	{
		head = new;
		head->next = NULL;
	}
	else
	{
		while(temp->next != NULL)
		{
			temp = temp->next;
		}
		temp->next = new;
		new->next = NULL;
	}
}

float tofloat(char buf[10])
{
	float n = 0, m = 0, x= .1;
	int len, i, flag = 0;
	len = strlen(buf);

	for(i = 0; i < len; i++)
	{
		if(buf[i] == '.')
		{
			flag = 1;
			i++;
		}

		if(flag == 0)
		{
			n = n * 10 + (buf[i] - '0');
		}
		else
		{
			m = m + (buf[i] - '0')*(x);
			x *= .1;
		}
	}
	n = n + m;
	return n;
}

void print(exchange_t *param)
{
	exchange_t *temp;
	temp = param;
	while(temp != NULL)
	{
		printf("%s     \t%.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f\n", temp->id, temp->a, temp->b, temp->c, temp->d, temp->e, temp->f, temp->g, temp->h, temp->i);
		temp = temp->next;
	}
}

void compute(exchange_t *param)
{
	exchange_t *temp;
	temp = param;
	float x[8], y[8], xy[8], xx[8], sumx, sumy, sumxy, sumxx, slope, inter, ave = 0;
	int i;

	while(temp != NULL)
	{
		sumx = 0;
		sumy = 0;
		sumxy = 0;
		sumxx = 0;
		slope = 0;
		inter = 0;

		if(temp->a == 0)
		{
			for(i = 0; i < 8; i++)
			{
				x[i] = (float)(i+1);
			}
			y[7] = temp->b;
			y[6] = temp->c;
			y[5] = temp->d;
			y[4] = temp->e;
			y[3] = temp->f;
			y[2] = temp->g;
			y[1] = temp->h;
			y[0] = temp->i;

			for(i = 0; i < 8; i++)
			{
				xy[i] = x[i]*y[i];
				xx[i] = x[i]*x[i];
			}

			for(i = 0; i < 8 ; i++)
			{
				sumx += x[i];
				sumy += y[i];
				sumxy += xy[i];
				sumxx += xx[i];
			}

			slope = ((8*sumxy) - (sumx*sumy))/((8*sumxx)-(sumx*sumx));
			inter = (sumy - (slope*sumx))/8;
			temp->a = (9*slope) + inter;
		}
		
		ave += temp->a;
		temp = temp->next;
	}
	ave -= head->a;
	ave /= 12;
	head->a = ave;
}

void finalcompute(exchange_t *param)
{
	exchange_t *temp;
	temp = param;
	char *month[13] = {"Average", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	int i, j = 0;
	float x[9], y[9], xy[9], xx[9], sumx, sumy, sumxy, sumxx, slope, inter, reg[13], ave = 0;

	while(temp != NULL)
	{
		sumx = 0;
		sumy = 0;
		sumxy = 0;
		sumxx = 0;
		slope = 0;
		inter = 0;

		for(i = 0; i < 9; i++)
		{
			x[i] = (float)i+1;
		}

		y[0] = temp->i;
		y[1] = temp->h;
		y[2] = temp->g;
		y[3] = temp->f;
		y[4] = temp->e;
		y[5] = temp->d;
		y[6] = temp->c;
		y[7] = temp->b;
		y[8] = temp->a;

		for(i = 0; i < 9; i++)
		{
			xy[i] = x[i]*y[i];
			xx[i] = x[i]*x[i];
		}

		for(i = 0; i < 9; i++)
		{
			sumx += x[i];
			sumy += y[i];
			sumxy += xy[i];
			sumxx += xx[i];
		}

		slope = ((9*sumxy) - (sumx*sumy))/((9*sumxx)-(sumx*sumx));
		inter = (sumy - (slope*sumx))/9;
		reg[j] = (10*slope) + inter;
		j++;
		temp = temp->next;
	}

	for(i = 1; i < 13; i++)
	{
		ave += reg[i];
	}

	ave /= 12;
	reg[0] = ave;
	printf("Predicted Exchange Rate in 2015:\n");

	for(i = 12; i > 0; i--)
	{
		printf("%s     \t%.3f\n", month[13-i], reg[i]);
	}
}

int main(void)
{
	FILE *f;
	char temp[256], temp1[10], hi[10];
	char s[2] = ",";
	char *token;
	int flag = 0, cnt;
	float a[9];

	f = fopen("exchange.csv", "r");

	if(!f)
	{
		printf("Error!\n");
		return 1;
	}	

	while(fgets(temp, 256, f) != NULL)
	{
		cnt = 0;
		if(flag > 2)
		{
			token = strtok(temp, s);
			strcpy(hi, token);
			while(token != NULL)
			{
				cnt++;
				if(cnt > 1)
				{
					strcpy(temp1, token);
					if(cnt == 10)
					{
						temp1[strlen(temp1)-2] = '\0';
					}
					a[cnt-2] = tofloat(temp1);
				}
				token = strtok(NULL, s);
			}
			add(hi, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]);
		}
		flag++;
	}
	compute(head);
	finalcompute(head);
	return 0;
}